# agent

## Context

This repository contains a ROS package that provides nodes to host a maze game. This Package is part of the AI Challenge 2019, an activity directed toward students from fifth semester and below from Universidad de los Andes. This Challenge is organized by the Uniandes Team from the SinfonIA alliance. This allience is an effort from academy and industry to implement and apply solutions to a variety of problems using artificial intelligence. At this moment, the institutions that belong to SINFONIA are: Universidad de los Andes, Universidad Santo Tomás, Universidad del Magdalena and Bancolombia.

## Description

This is a version of a maze puzzle implemented over ROS (Robot Operating System, http://www.ros.org/). The whole maze game for the AI Challenge 2019 consists of two packages: _maze_env_, which contains the maze enviroment and _agent_ wich contains the agent that controls the movement within the maze. 

The first one hosts a ROS node (_env_maze_) that acts as a simulator of a predefined maze. This node publishes in a ROS topic (_/position_) the position of the agent in the maze. Similiarly, the second package also hosts a ROS node (_agent_) which comunicates with the first node (_env_maze_) by publishing in a ROS topic (_/action_) the information needed to move throughout the maze. The ROS node _env_maze_ is, therfore, subscribed to the ROS topic _action_. It is excpected that the agent node generates the actions needed to solve the maze by reading the position from the env_maze node and publishing the next movement in the _/action_ topic. Furthermore, the information of the maze (obstacles) can be retrieved as a ROS service (_Mapinfo_)

## System Requirements

	- Ubuntu 16.04 64 bits
	- ROS Kinetic
	- pygame

## Installation
	There are two ways to install all the dependencies for the maze software:
	1) The user can manually install ROS (Kinetic Kame) and the corresponding catkin workspace following the tutorials in http://wiki.ros.org/kinetic/Installation/Ubuntu
	2) There is a script that installs all the dependencies including ROS and the catkin workspace (it takes some time). The script also installs git, however it is not mandatory. To run it, execute the following steps:
	   a) Download and extract the maze repository from https://gitlab.com/maze_challenge/maze_env
	   b) Open a terminal
	   c) cd to the repository folder
	   d) source installRequirements.sh
				
## Compilation
	If the user decides to install manually the requirements, then he or she should execute the following lines in a new terminal (or follow the steps) to compile the proyect:
	- cd ~/catkin_ws/src
	- git clone https://gitlab.com/maze_challenge/maze_env (if git is installed. Otherwise, just download the repository folder into the specified folder)
	- git clone https://gitlab.com/maze_challenge/agent (if git is installed. Otherwise, just download the repository folder into the specified folder)
	- cd ..
	- source devel/setup.bash
	- catkin_make

## Execution
There are two ways to execute the env_maze node and the agent node. 1) By executing it directly in one machine as a traditional ROS node and 2) Using the launch file. The first option requires the user to independently run a roscore process, source the setup.bash and run the env_maze node using the rosrun command.

TRADITIONAL EXECUTION 

	- Open a new terminal
	- Run: roscore
	- Open a new terminal
	- cd catkin_ws
	- source devel/setup.bash
	- rosrun maze_env env_maze.py
	- Open a new terminal
	- cd catkin_ws
	- source devel/setup.bash
	- rosrun agent agent.py

USING THE LAUNCH FILE

	- Open a new terminal
	- cd catkin_ws
	- source devel/setup.bash
	- roslaunch maze_env maze.launch