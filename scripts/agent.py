#! /usr/bin/env python3
import rospy
from std_msgs.msg import String
from maze_env.srv import MapInfo, MapInfoResponse
from random import randrange

class Agent():

	def __init__(self):                 #Initialize variables
		self.msg_action = String
		self.num_obstacles = None
		self.pos_obstacles = None
		self.start = None
		self.goal = None
		

	def number_to_action(self,number):  #Define cases in which an action is represented by a number
		if number==0:                   
			self.msg_action = 'stay'
		elif number==1:
			self.msg_action = 'up'
		elif number==2:
			self.msg_action = 'right'
		elif number==3:
			self.msg_action = 'down'
		else:
			self.msg_action = 'left'
		return self.msg_action

	def get_map_info(self):
		rospy.wait_for_service('map_info')   #Wait for map information service
		try:
			map_service = rospy.ServiceProxy('/map_info', MapInfo)
			data = map_service("AIChallenge")          #Get information from map 
			self.num_obstacles = data.n_obstacles
			self.pos_obstacles = data.obstacle_list
			self.start = data.start
			self.goal = data.goal
			print(self.num_obstacles)
			print(self.pos_obstacles)
			print(self.start)
			print(self.goal)
		except rospy.ServiceException:
			print("Service call failed:")

	def main(self):
		rospy.init_node('agent')           #Initialize node
		actionPub = rospy.Publisher('action', String, queue_size=10)    #Create publishers
		self.get_map_info()                #Get map information
		rate = rospy.Rate(1)
		while not rospy.is_shutdown():
			# Enter code here. Use variables: --- self.num_obstacles, self.pos_obstacle, self.start and self.goal to develop your algorithm
			action = randrange(5)        #Generate a random number 
			self.msg_action = self.number_to_action(action)   #Assign the random number to an action
			# In variable msg_action you must save the number that will give the instruction for the agent's movement.
			actionPub.publish(self.msg_action)    #Publish action
			rospy.loginfo(self.msg_action)
			rate.sleep()

if __name__ == "__main__":
	agent = Agent()
	agent.main()
